import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'

import { EmployeeService } from '../../services/employee.service'
import {Employee} from '../../interfaces/Employee'

@Component({
  selector: 'app-employee-preview',
  templateUrl: './employee-preview.component.html',
  styleUrls: ['./employee-preview.component.css']
})
export class EmployeePreviewComponent implements OnInit {

  id: string;
  employee: Employee;

  constructor(
    private activatedRoute: ActivatedRoute,
    private employeeService: EmployeeService,
    private router: Router
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.id = params['id'];
      this.employeeService.getEmployee(this.id)
        .subscribe(
          res => {
            this.employee = res;
          },
          err => console.log(err)
        )
    });
  }

  deleteEmployee(id: string) {
    this.employeeService.deleteEmployee(id)
      .subscribe(res => {
        console.log(res)
        this.router.navigate(['/employees']);
      })
  }

  updateEmployee(name: HTMLInputElement, contractTypeName: HTMLInputElement, roleId: HTMLInputElement, roleName: HTMLInputElement, roleDescription: HTMLInputElement, hourlySalary: HTMLInputElement, monthlySalary: HTMLInputElement): boolean {
    this.employeeService.updateEmployee(this.employee._id, name.value, contractTypeName.value, roleId.value, roleName.value, roleDescription.value, hourlySalary.value, monthlySalary.value)
      .subscribe(res => {
        console.log(res);
        this.router.navigate(['/employees']);
      });
    return false;
  }

}