import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

import { EmployeeService } from '../../services/employee.service'
import { Employee } from '../../interfaces/Employee'

@Component({
  selector: 'app-employees-list',
  templateUrl: './employees-list.component.html',
  styleUrls: ['./employees-list.component.css']
})
export class EmployeesListComponent implements OnInit {

  employees: Employee[] = [];

  constructor(
    private employeeService: EmployeeService,
    private router: Router 
  ) { }

  ngOnInit() {
    this.employeeService.getEmployees()
      .subscribe(
        res => {
          this.employees = res;
        },
        err => console.log(err)
      )
  }

  selectedCard(id: string) {
    this.router.navigate(['/employees', id]);
  }

}