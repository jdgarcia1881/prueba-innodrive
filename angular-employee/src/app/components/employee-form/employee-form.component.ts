import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router'
import {EmployeeService} from '../../services/employee.service'

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.css']
})
export class EmployeeFormComponent implements OnInit {
  constructor(private employeeService: EmployeeService, private router: Router) { }

  ngOnInit(): void {
  }

  registerEmployee(name: HTMLInputElement, contractTypeName: HTMLInputElement, roleId: HTMLInputElement, roleName: HTMLInputElement, roleDescription: HTMLInputElement, hourlySalary: HTMLInputElement, monthlySalary: HTMLInputElement) {
    this.employeeService
      .createEmployee(name.value, contractTypeName.value, roleId.value, roleName.value, roleDescription.value, hourlySalary.value, monthlySalary.value)
      .subscribe(
        res => {
          console.log(res);
          this.router.navigate(['/employees'])
        },
        err => console.log(err)
      );
    return false;
  }
}
