import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmployeesListComponent } from './components/employees-list/employees-list.component'
import { EmployeeFormComponent } from './components/employee-form/employee-form.component'
import { EmployeePreviewComponent } from './components/employee-preview/employee-preview.component'

const routes: Routes = [
  {
    path: 'employees',
    component: EmployeesListComponent
  },
  {
    path: 'employees/new',
    component: EmployeeFormComponent
  },
  {
    path: 'employees/:id',
    component: EmployeePreviewComponent
  },
  {
    path: '',
    redirectTo: '/employees',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }