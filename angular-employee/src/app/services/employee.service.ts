import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'

import {Employee} from '../interfaces/Employee'

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  URI = 'http://localhost:4000/api/employees'

  constructor(private http: HttpClient) { }

  createEmployee(name: string, contractTypeName: string, roleId: string,roleName: string, roleDescription: string, hourlySalary: string, monthlySalary: string) {
    const fd = new FormData();
    fd.append('name', name);
    fd.append('contractTypeName', contractTypeName);
    fd.append('roleId', roleId);
    fd.append('roleName', roleName);
    fd.append('roleDescription', roleDescription);
    fd.append('hourlySalary', hourlySalary);
    fd.append('monthlySalary', monthlySalary);
    return this.http.post(this.URI, fd)
  }

  getEmployees() {
    return this.http.get<Employee[]>(this.URI);
  }

  getEmployee(id: string) {
    return this.http.get<Employee>(`${this.URI}/${id}`);
  }

  deleteEmployee(id: string) {
    return this.http.delete(`${this.URI}/${id}`);
  }

  updateEmployee(id: string, name: string, contractTypeName: string, roleId: string,roleName: string, roleDescription: string, hourlySalary: string, monthlySalary: string) {
    return this.http.put(`${this.URI}/${id}`, {name, contractTypeName, roleId, roleName, roleDescription, hourlySalary, monthlySalary});
  }
}
