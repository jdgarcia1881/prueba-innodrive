import {connect} from 'mongoose'

export async function startConnection () {
    await connect('mongodb://localhost/employee-innodrive-db', {
        useNewUrlParser: true,
        useFindAndModify: false
    });
    console.log('Database is connected')
}