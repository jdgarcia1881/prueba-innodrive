import {Schema, model, Document} from 'mongoose';

const schema = new Schema({
    name: String,
    contractTypeName: String,
    roleId: Number,
    roleName: String,
    roleDescription: String,
    hourlySalary: Number,
    monthlySalary: Number
});

interface IEmployee extends Document {
    name: string;
    contractTypeName: string;
    roleId: number;
    roleName: string;
    roleDescription: string;
    hourlySalary: number;
    monthlySalary: number;
}

export default model<IEmployee>('Employee', schema);