import {Request, Response} from 'express'

//Models
import Employee from '../models/Employee';

export async function createEmployee(req: Request, res: Response): Promise<Response> {
    const { name, contractTypeName, roleId, roleName, roleDescription, hourlySalary, monthlySalary } = req.body;
    const newEmployee = {
        name: name,
        contractTypeName: contractTypeName,
        roleId: roleId,
        roleName: roleName,
        roleDescription: roleDescription,
        hourlySalary: hourlySalary,
        monthlySalary: monthlySalary
    };
    const employee = new Employee(newEmployee);
    await employee.save();
    return res.json({
        message: 'Employee successfully saved',
        employee
    })
}

export async function getEmployees(req: Request, res: Response): Promise<Response> {
    const employees = await Employee.find();
    return res.json(employees);
}

export async function getEmployee(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;
    const employee = await Employee.findById(id);
    return res.json(employee);
}

export async function deleteEmployee(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;
    const employee = await Employee.findByIdAndRemove(id);
    return res.json({ 
        message: 'Employee Deleted', 
        employee 
    });
};

export async function updateEmployee(req: Request, res: Response): Promise<Response> {
    const { id } = req.params;
    const { name, contractTypeName, roleId, roleName, roleDescription, hourlySalary, monthlySalary } = req.body;
    console.log(req.body);
    const updateEmployee = await Employee.findByIdAndUpdate(id, {
        name,
        contractTypeName,
        roleId,
        roleName,
        roleDescription,
        hourlySalary,
        monthlySalary
    }, {new: true});
    return res.json({
        message: 'Successfully updated',
        updateEmployee
    });
}