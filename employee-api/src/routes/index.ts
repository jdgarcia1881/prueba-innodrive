import {Router} from 'express';
const router = Router();

import {createEmployee, getEmployees, getEmployee, deleteEmployee, updateEmployee} from '../controllers/employee.controller'

router.route('/employees')
    .get(getEmployees)
    .post(createEmployee)

router.route('/employees/:id')
    .get(getEmployee)
    .delete(deleteEmployee)
    .put(updateEmployee)

export default router;