"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateEmployee = exports.deleteEmployee = exports.getEmployee = exports.getEmployees = exports.createEmployee = void 0;
//Models
const Employee_1 = __importDefault(require("../models/Employee"));
async function createEmployee(req, res) {
    const { name, contractTypeName, roleId, roleName, roleDescription, hourlySalary, monthlySalary } = req.body;
    const newEmployee = {
        name: name,
        contractTypeName: contractTypeName,
        roleId: roleId,
        roleName: roleName,
        roleDescription: roleDescription,
        hourlySalary: hourlySalary,
        monthlySalary: monthlySalary
    };
    const employee = new Employee_1.default(newEmployee);
    await employee.save();
    return res.json({
        message: 'Employee successfully saved',
        employee
    });
}
exports.createEmployee = createEmployee;
async function getEmployees(req, res) {
    const employees = await Employee_1.default.find();
    return res.json(employees);
}
exports.getEmployees = getEmployees;
async function getEmployee(req, res) {
    const { id } = req.params;
    const employee = await Employee_1.default.findById(id);
    return res.json(employee);
}
exports.getEmployee = getEmployee;
async function deleteEmployee(req, res) {
    const { id } = req.params;
    const employee = await Employee_1.default.findByIdAndRemove(id);
    return res.json({
        message: 'Employee Deleted',
        employee
    });
}
exports.deleteEmployee = deleteEmployee;
;
async function updateEmployee(req, res) {
    const { id } = req.params;
    const { name, contractTypeName, roleId, roleName, roleDescription, hourlySalary, monthlySalary } = req.body;
    console.log(req.body);
    const updateEmployee = await Employee_1.default.findByIdAndUpdate(id, {
        name,
        contractTypeName,
        roleId,
        roleName,
        roleDescription,
        hourlySalary,
        monthlySalary
    }, { new: true });
    return res.json({
        message: 'Successfully updated',
        updateEmployee
    });
}
exports.updateEmployee = updateEmployee;
